package com.example.baselibrary.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter{
    public List<ViewTypeModel> dataModels;
    public Context mContext;
    public int total_types;
    public RecyclerView.ViewHolder[] viewHolders;
    public AdapterClickLisenter lisenter;
    public BaseRecyclerAdapter(Context mContext){
        this.mContext=mContext;
        dataModels=new ArrayList<>();
    }

    public void setLisenter(AdapterClickLisenter lisenter){
        this.lisenter=lisenter;
    }

    public void setData(List<ViewTypeModel> data){
        this.dataModels=data;
        total_types=data.size();
        notifyDataSetChanged();
    }

    public void addItem(ViewTypeModel model){
        dataModels.add(model);
        notifyItemInserted(dataModels.size()-1);
    }

    public void setViewHolders(RecyclerView.ViewHolder... viewHolders){
        this.viewHolders=viewHolders;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflateView(LayoutInflater.from(mContext),parent,viewType);
        Log.d("sizeee",viewType+"");

        return viewHolders[viewType];
    }

    public abstract void inflateView(LayoutInflater inflater,ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    public ViewTypeModel getValueAt(int po){
        return dataModels.get(po);
    }

    @Override
    public int getItemViewType(int position) {
        return dataModels.get(position).getType();
    }

    @Override
    public int getItemCount() {
        Log.d("sizeee",dataModels.size()+"");
        return dataModels.size();
    }

    public interface  AdapterClickLisenter{
        void onClickItem(int pos);
    }
}
