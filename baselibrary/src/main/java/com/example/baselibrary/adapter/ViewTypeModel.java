package com.example.baselibrary.adapter;

public class ViewTypeModel<data>{
    private data data;
    private int type;

    private ViewTypeModel(ViewTypeBuilder<data> builder){
        this.data= builder.data;
        this.type=builder.type;
    }

    public data getData() {
        return data;
    }

    public int getType() {
        return type;
    }

    public static class ViewTypeBuilder<data>{
        private data data;
        private int type;

        public ViewTypeBuilder(data data){
            this.data=data;
            this.type=0;
        }

        public ViewTypeBuilder setType(int type){
            this.type=type;
            return this;
        }

        public ViewTypeModel build(){
            return new ViewTypeModel<data>(this);
        }
    }

    @Override
    public String toString() {
        return "ViewTypeModel{" +
                "data=" + data +
                ", type=" + type +
                '}';
    }
}
