package com.example.baselibrary.api.network;

public interface OnCheckConnection {
    void ConnectionTrue();
    void ConnectionError();
}
