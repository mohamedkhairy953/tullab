package com.example.baselibrary.api.retrofit;

import android.content.Context;

import com.example.baselibrary.R;
import com.example.baselibrary.api.network.CheckNetwork;
import com.example.baselibrary.listener.RequestListener;
import com.google.gson.JsonSyntaxException;

import javax.net.ssl.SSLHandshakeException;

import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

public class Request<data> extends DisposableObserver<data>{
    private RequestListener<data> requestListener;
    private Context context;
    public Request(RequestListener<data> listener, Context context){
        this.context=context;
        this.requestListener=listener;
    }
    @Override
    public void onNext(data data) {
     if (data!=null){
         requestListener.onResponse(data);
     }else {
         requestListener.onEmptyData("No Data Found");
     }
    }

    @Override
    public void onError(Throwable e) {
        if (!CheckNetwork.isConnected(context)) {
            requestListener.onNetWorkError();
        } else {

            if (e instanceof HttpException) {
                int code = ((HttpException) e).code();
                if (code == 401 || code == 400) {
                    requestListener.onSessionExpired(context.getString(R.string.invalid_credentials));
                } else if (code == 504) {
                    requestListener.onResponseError(context.getString(R.string.timeout));
                } else {
                    requestListener.onResponseError(context.getString(R.string.serverError));
                }
            } else if (e instanceof java.net.SocketTimeoutException) {
                requestListener.onResponseError(context.getString(R.string.socketTimeout));
            }
            else if (e instanceof JsonSyntaxException) {
                requestListener.onResponseError(context.getString(R.string.jsonError));
            }
            else if (e instanceof SSLHandshakeException) {
                requestListener.onResponseError(context.getString(R.string.connectionError));
            }
            else {
                //    Log.d("onError", "Throwable " + t);
                requestListener.onResponseError(context.getString(R.string.serverError));
            }
        }
    }

    @Override
    public void onComplete() {
       requestListener.onComplete();
    }
}
