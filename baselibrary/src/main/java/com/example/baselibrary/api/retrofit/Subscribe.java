package com.example.baselibrary.api.retrofit;

import android.annotation.SuppressLint;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class Subscribe{

    private static Subscribe subscribe;
    private CompositeDisposable compositeDisposable=new CompositeDisposable();

    public static Subscribe With(){
        if (subscribe==null){
            synchronized (Subscribe.class){
                subscribe=new Subscribe();
            }
        }
        return subscribe;
    }
    @SuppressLint("CheckResult")
    public <data> void Subscription(Observable<data> observable, DisposableObserver<data> observer){
        observable.observeOn(AndroidSchedulers.mainThread());
        observable.subscribeOn(Schedulers.io());
        observable.subscribe(observer);
    }
}
