package com.example.baselibrary.listener;

public interface RetryListener {
    void onRetry();
}
