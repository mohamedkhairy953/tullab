package com.example.baselibrary.listener;

public interface RequestListener<data> {

    void onResponse(data data);
    void onFailed(String msg);
    void onEmptyData(String msg);

    void onSessionExpired(String msg);

    void onResponseError(String msg);

    void onNetWorkError();

    void onComplete();
}
