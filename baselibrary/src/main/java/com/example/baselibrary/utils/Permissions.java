package com.example.baselibrary.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

public class Permissions {
    private static Permissions mPermissions;
    private Activity activity;

    public static Permissions getPermissions(Activity activity){
        if (mPermissions==null){
            synchronized (Permissions.class){
                mPermissions=new Permissions(activity);
            }
        }
        return mPermissions;
    }

    private Permissions(Activity activity){
        this.activity=activity;
    }

    public boolean hasPermission(String permission) {
        return ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED;
    }

    public void checkPermissions(int permissionCode ,String ... permissions) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                for (int i = 0; i < permissions.length; i++) {
                    if (hasPermission(permissions[i])) {
                        activity.requestPermissions(permissions, permissionCode);
                    }
                }
            }
        } catch (Exception ignored) {
        }

    }


}
