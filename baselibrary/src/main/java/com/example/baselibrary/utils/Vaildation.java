package com.example.baselibrary.utils;


import android.widget.EditText;

public class Vaildation {

    public boolean isEmailValid(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public boolean isEmailValid(String email, EditText editText, String errorMsg) {
        if (isEmailValid(email)) {
            return true;
        } else {
            editText.setError(errorMsg);
            return false;
        }
    }


    public boolean isPasswordValid(String password) {
        return !password.isEmpty() && password.length() > 4;
    }

    public boolean isPasswordValid(String password, EditText editText, String errorMsg) {
        if (isUserNameValid(password)) {
            return true;
        } else {
            editText.setError(errorMsg);
            return false;
        }
    }

    public boolean isPhoneValid(String phone) {
        return !phone.isEmpty() && phone.length() >= 10;
    }

    public boolean isPhoneValid(String phone, EditText editText, String errorMsg) {

        if (isPhoneValid(phone)) {
            return true;
        } else {
            editText.setError(errorMsg);
            return false;
        }

    }

    public boolean isAddressValid(String address) {
        return !address.isEmpty() && address.length() > 15;
    }

    public boolean isAddressValid(String address, EditText editText, String errorMsg) {

        if (isAddressValid(address)) {
            return true;
        } else {
            editText.setError(errorMsg);
            return false;
        }

    }


    public boolean checkIdNumber(String s) {
        return (s != null &&
                (!s.isEmpty()) &&
                !(s.equals("null")) &&
                !s.equals("") &&
                !s.equals(" "))
                && s.length() == 10;
    }


    public boolean checkIdNumber(String IdNumber, EditText editText, String errorMsg) {
        if (checkIdNumber(IdNumber)) {
            return true;
        } else {
            editText.setError(errorMsg);
            return false;
        }
    }


    public boolean check(String s) {
        return (s != null &&
                (!s.isEmpty()) &&
                !(s.equals("null")) &&
                !s.equals("") &&
                !s.equals(" "));
    }


    public boolean checkComment(String comment, EditText editText, String errorMsg) {

        if (check(comment)) {
            return true;
        } else {
            editText.setError(errorMsg);
            return false;
        }
    }



    public boolean isNameValid(String fullName) {
        return java.util.regex.Pattern.compile("^[a-zA-Z\u0621-\u064A ]{3,25}$").matcher(fullName).matches();
    }

    public boolean isNameValid(String fullName, EditText editText, String errorMsg) {
        if (isNameValid(fullName)) {
            return true;
        } else {
            editText.setError(errorMsg);
            return false;
        }
    }

    public boolean isUserNameValid(String userName) {
        String USERNAME_PATTERN = "^[a-zA-Z0-9_-]{3,30}$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(USERNAME_PATTERN);
        java.util.regex.Matcher m = p.matcher(userName);
        return m.matches();
    }

    public boolean isUserNameValid(String userName, EditText editText, String errorMsg) {

        if (isUserNameValid(userName)) {
            return true;
        } else {
            editText.setError(errorMsg);
            return false;
        }
    }


    public boolean isNameValidWithEmpty(String s, EditText editText, String error) {
        return (!Is.nn(s)) || isNameValid(s, editText, error);

    }


    public boolean isAddressValidWithEmpty(String s, EditText editText, String error) {
        if (Is.nn(s))
            return isAddressValid(s, editText, error);
        return true;
    }

    public boolean isPhoneValidWithEmpty(String s, EditText editText, String error) {
        if (Is.nn(s))
            return isPhoneValid(s, editText, error);
        return true;
    }

    public boolean isEmailValidWithEmpty(String s, EditText editText, String error) {
        if (Is.nn(s)) {
            return isEmailValid(s, editText, error);
        }
        return true;
    }

}
