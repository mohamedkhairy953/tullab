package com.example.baselibrary.ui.fragment;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment implements View.OnClickListener{
    public View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(setContentView(),container,false);
        iniLoad();
        viewReady(savedInstanceState);
        iniViews();
        return view;
    }


    public <t extends View> t bind(@IdRes int id){
        return view.findViewById(id);
    }

    public void click(@IdRes int ... ids){
        for (int id:ids) {
            bind(id).setOnClickListener(this);
        }
    }

    public void click(@Nullable View ... views){
        for (View view:views) {
            view.setOnClickListener(this);
        }
    }
    public abstract void viewReady(Bundle bundle);
    public abstract int setContentView();
    public abstract void iniViews();

    public void iniLoad(){}
}
