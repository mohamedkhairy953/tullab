package com.example.baselibrary.ui.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.Locale;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener{

    public enum Languages{
        ar,
        en,

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setContentView());
        iniLoader();
        onViewReady(savedInstanceState);
        loadLanuage();
        iniViews();
    }






    public void loadLanuage(){
        if (SetLanguage()==null){
            setupLanguage("en");
            return;
        }
        switch (SetLanguage()){
            case ar:
                setupLanguage("ar");
                break;
            case en:
                setupLanguage("en");
                return;
            default:
                setupLanguage("en");
                break;
        }
    }


    public void iniLoader(){}

    public <t extends View> t bind(@IdRes int  id){
          return findViewById(id);
    }

    public void click(@IdRes int ... ids){
        for (int id:ids) {
          bind(id).setOnClickListener(this);
        }
    }

    public void click(@Nullable View ... views){
        for (View view:views) {
           view.setOnClickListener(this);
        }
    }





    public abstract int setContentView();

    public abstract void iniViews();
    public abstract void onViewReady(@Nullable Bundle bundle);

    public abstract Languages SetLanguage();

    private void setupLanguage(String language){
        updateResources(getApplicationContext(),language);
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale);
            config.setLayoutDirection(locale);

        } else {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
    }

}
