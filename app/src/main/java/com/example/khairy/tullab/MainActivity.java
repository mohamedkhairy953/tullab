package com.example.khairy.tullab;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.baselibrary.ui.activity.BaseActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends BaseActivity {



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public int setContentView() {
        return R.layout.ordering_soc05;
    }

    @Override
    public void iniViews() {

    }

    @Override
    public void onViewReady(@Nullable Bundle bundle) {

    }

    @Override
    public Languages SetLanguage() {
        return Languages.ar;
    }

    @Override
    public void onClick(View v) {

    }
}
