package com.example.khairy.tullab.application;

import android.app.Application;


import com.example.khairy.tullab.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/arabia.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


    }
}
